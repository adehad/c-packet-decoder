﻿#include <stdio.h>

#include "PacketProcessor.c"

/**
 * @brief Accepts a filename argument for a binary file to parse.
 *
 * @param argc Argument count.
 * @param argv [1]: The binary filename/path to parse.
 * @return int Exit Code.
 */
int main(int argc, char* argv[]) {
  int is_ok = EXIT_FAILURE;
  if (argc < 2) {
    puts("ERR:Must specify a filename argument!");
    return is_ok;
  }

  char* filename = argv[1];
  FILE* fp = fopen(filename, "rb");
  if (!fp) {
    perror("ERR:File opening failed");
    return is_ok;
  }

  packet_decoder(fp);

  if (feof(fp)) {
    is_ok = EXIT_SUCCESS;
  } else {
    puts("ERR:Did not reach end of file reached successfully");
  }

  fclose(fp);
  return is_ok;
}

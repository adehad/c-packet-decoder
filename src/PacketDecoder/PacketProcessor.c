
#include "../../include/PacketProcessor.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef DEBUG
#define DEBUG 0
#endif  // !DEBUG

#ifndef DEBUG_PRINT
#define DEBUG_PRINT(fmt, ...)                                           \
  do { /* https://stackoverflow.com/a/1644898/7724187 */                \
    if (DEBUG > 2) {                                                    \
      fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, \
              __VA_ARGS__);                                             \
    } else if (DEBUG > 0) {                                             \
      fprintf(stderr, fmt, __VA_ARGS__);                                \
    }                                                                   \
  } while (0)
#endif  // !DEBUG_PRINT

enum CONSTANTS {
  NULL_VALUE,
};

/**
 * @brief Debug printing of values in the PowerLevelStruct_t.
 *
 * @param pStruct Pointer to a PowerLevelStruct_t.
 */
void display_power(pPowerLevelStruct_t pStruct) {
  DEBUG_PRINT("%lu \t%lu \t%llu \t%i \n", pStruct->time_ms, pStruct->voltage_V,
              pStruct->current_mA, pStruct->error_check);
}

/**
 * @brief Debug printing of values in the BatteryInfoStruct_t.
 *
 * @param pStruct Pointer to a BatteryInfoStruct_t.
 */
void display_battery(pBatteryInfoStruct_t pStruct) {
  DEBUG_PRINT("%lu \t%i \t%i \n", pStruct->time_ms, pStruct->battery_status,
              pStruct->error_check);
}

/**
 * @brief The main packet decoder and emitter.
 *
 * The decoder reads a single byte to determine the packet type.
 * Bytes are read to match the expected packet type payload.
 * The byte arrays are processed and valid transitions are output to stdout.
 *
 * @param fp The file pointer to consume data from.
 */
void packet_decoder(FILE* fp) {
  char sensor_buff[sizeof(PowerLevelStruct_t)];
  char battery_buff[sizeof(BatteryInfoStruct_t)];
  pPowerLevelStruct_t previous_sensor_data_struct =
      &(PowerLevelStruct_t){.time_ms = NULL_VALUE,
                            .voltage_V = NULL_VALUE,
                            .current_mA = NULL_VALUE,
                            .error_check = NULL_VALUE};
  pPowerLevelStruct_t sensor_data_struct = NULL;
  pBatteryInfoStruct_t battery_data_struct = NULL;
  char packet_type_buff[1];
  uint8_t packet_type;
  while ((fread(packet_type_buff, sizeof(*packet_type_buff),
                sizeof(packet_type), fp))) {
    packet_type = (uint8_t)packet_type_buff[0];
    switch (packet_type) {
      case PowerLevel:
        DEBUG_PRINT("sensor\t");
        fread(sensor_buff, sizeof(*sensor_buff), sizeof(sensor_buff), fp);
        pPowerLevelStruct_t sensor_data_struct =
            (pPowerLevelStruct_t)sensor_buff;
        convert_power(sensor_data_struct);
        display_power(sensor_data_struct);
        print_power_packet(previous_sensor_data_struct, sensor_data_struct,
                           stdout);
        break;
      case BatteryInfo:
        DEBUG_PRINT("battery\t");
        fread(battery_buff, sizeof(*battery_buff), sizeof(battery_buff), fp);
        pBatteryInfoStruct_t battery_data_struct =
            (pBatteryInfoStruct_t)battery_buff;
        convert_battery(battery_data_struct);
        display_battery(battery_data_struct);
        print_battery_packet(battery_data_struct, stdout);
        break;
      default:
        printf("ERR:Unhandled packet type: '%d'\n", packet_type);
        break;
    }
    if (ferror(fp)) {
      puts("ERR:I/O error when reading");
      break;
    }
  }
}

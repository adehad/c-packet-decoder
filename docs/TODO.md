# TODOs

This list aims to capture the technical debt and other incomplete work.
Some lightweight management of priorities is captured through the ordering of the list.

- [x] Find a suitable project template to copy -- Jason Turner `cpp_starter_project`
- [x] Find a suitable test framework to use -- gtest
- [x] Refactor to use headers -- `clang-tidy` isn't happy that they have the implementation inside.
- [x] Add a README with build instructions.
- [x] Add docstrings.
- [x] Add some linter config.
- [x] Add some pseudocode tests in the GIVEN,WHEN,THEN format.
- [x] Error check
  - [ ] Figure out why the error check doesn't add up.
- [x] Integrations tests - using a binary.
- [ ] Should not need to call `convert_xyz()` manually,
      instead a pointer and the buffer should be the arguments to a function,
      the pointer's value then should be updated, with the conversion an
      implementation detail.
- [ ] The valid power state ranges don't match the requirements.
- [ ] Why didn't the static pointer initialisation work in `print_power_packet()` for the previous packet?
- [ ] Find out how to fix the compile warnings for the tests to work.
- [ ] Create a top level Makefile to automate running `cmake` build steps.
- [ ] Use more portable byteswap code.
- [ ] GitLab pipelines for runnings lint and build?


## Questions that should have been asked to the client:

1. What is the code formatting standard or guidelines for projects? Is there an existing new project template?
2. Is there a target platform/compiler that needs to be accounted for?

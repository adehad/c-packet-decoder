#include <gtest/gtest.h>

// #include "../src/PacketDecoder/PacketProcessor.c"

TEST(BatteryPacketTests, Size) {
  // GIVEN: BatteryInfoStruct
  // WHEN: The sizeof() is checked
  // THEN: The size matches our expectation: 4
}

TEST(BatteryPacketTests, Parse) {
  // GIVEN: BatteryInfoStruct and a known byte array.
  // WHEN: The byte array to type cast to the Struct.
  // THEN: The value of the struct fields match our expectation.
}

TEST(BatteryPacketTests, Conversion) {
  // GIVEN: convert_battery() function and a known BatteryInfoStruct
  // WHEN: convert_battery() is called
  // THEN: The value of the fields match our expectation.
}

TEST(BatteryPacketTests, ErrorCheck) {
  // GIVEN: is_battery_packet_error_free() function and a known
  // BatteryInfoStruct WHEN: is_battery_packet_error_free() is called on a
  // correct/incorrect struct THEN: The error check returns the correct True or
  // False value.
}

TEST(BatteryPacketTests, Output) {
  // GIVEN: a BatteryInfoStruct for the 4 possible battery states.
  // WHEN: print_battery_packet() is called on this packet.
  // THEN: The expected output is received.
}

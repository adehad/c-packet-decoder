#include <gtest/gtest.h>

// #include "../src/PacketDecoder/PacketProcessor.c"

TEST(PowerSensorPacketTests, Size) {
  // GIVEN: PowerLevelStruct
  // WHEN: The sizeof() is checked
  // THEN: The size matches our expectation: 17
}

TEST(PowerSensorPacketTests, Parse) {
  // GIVEN: PowerLevelStruct and a known byte array.
  // WHEN: The byte array to type cast to the Struct.
  // THEN: The value of the struct fields match our expectation.
}

TEST(PowerSensorPacketTests, Conversion) {
  // GIVEN: a PowerLevelStruct.
  // WHEN: power_state() is called
  // THEN: The power state enum returned matches out expectation.
}

TEST(PowerSensorPacketTests, ErrorCheck) {
  // GIVEN: is_power_packet_error_free() function and a known PowerLevelStruct
  // WHEN: is_power_packet_error_free() is called on a correct/incorrect struct
  // THEN: The error check returns the correct True or False value.
}

TEST(PowerSensorPacketTests, PowerState) {
  // GIVEN: a PowerLevelStruct.
  // WHEN: valid_power_state_transition() is called.
  // THEN: Returns True on the valid transitions, false otherwise.
}

TEST(PowerSensorPacketTests, StateMachine) {
  // GIVEN: a pair of PowerLevelStruct states.
  // WHEN: valid_power_state_transition() is called.
  // THEN: Returns True on the valid transitions, false otherwise.
}

TEST(PowerSensorPacketTests, Output) {
  // GIVEN: a pair of PowerLevelStructs spaced sufficiently apart in time.
  // WHEN: print_power_packet() is for each packet.
  // THEN: The appropriate message is output.
}

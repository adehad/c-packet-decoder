#include <gtest/gtest.h>

// #include "../src/PacketDecoder/PacketProcessor.c"

TEST(IntegrationTests, ValidBinary) {
  // GIVEN: A valid binary and the expected output.
  // WHEN:
  //   * The the binary is passed to the application
  //   * The output is redirected.
  // THEN: The redirected output matches the expected output.
}

TEST(IntegrationTests, TooShortBinary) {
  // GIVEN: A too short binary, not enough bytes for any packet.
  // WHEN: the packet is attempted to be decoded.
  // THEN: The application outputs the expected error message.
}

# Packet Decoder Problem

The candidate has time-boxed the implementation.

## Documents

* The original problem statement document can be found in: `docs/Coding Test-ver-G.docx`
* The original binary supplied with the problem statement is found in the project root: `CodingTest.bin`
* The requirements generated from the problem statement can be found in `docs/Requirements.docx`
* The backlog / technical debt can be found in `docs/TODO.md`

## Install Instructions

### Windows

#### Pre-requisites:

Using [`chocolatey`](https://chocolatey.org/) and [`pipx`](https://pypa.github.io/pipx/#install-pipx):

```powershell
choco install cmake
choco install visualstudio2022community
choco install llvm

# for formatting CMake files - `cmake-format`.
pipx install cmakelang
```

#### Running the application

Open the project folder in Visual Studio Community.

It should automatically detect the `CMakeSettings.json` file that
will use the `Ninja` build system and the `msvc` compiler.

**NOTE:** The `msvc` compiler is required due to the use of non-portable code used
in the conversion from network to host byte order (e.g. `_byteswap_ulong()`).

This repository includes the configuration for Visual Studio to run the executable in:
`.vs/launch.vs.json`. This should automatically be picked up and be selectable in the
debug drop down.

The `args` specify what arguments are passed to the compiled executable, i.e. the binary file to test.

After compilation the executable will be found in:
`/out/build/x64-Debug/src/PacketDecoder/CodingTest.exe`

Re-saving any `CMakeLists.txt` file will automatically trigger the CMake build and compilation steps.

The tests are currently built and run with the application build.

## Tests

The `test/` folder includes the tests. With the `docs/Requirements.docx`
matching the requirements to the tests.

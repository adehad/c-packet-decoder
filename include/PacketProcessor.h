#include <stdint.h>
#include <stdio.h>

#include "PacketTypes/Battery.h"
#include "PacketTypes/PowerSensor.h"

#ifndef TIME_FMT
#define TIME_FMT "%.0f"
#endif  // !TIME_FMT

/**
 * @brief The packet types that can be processed.
 *
 */
enum PacketType {
  PowerLevel = 0x00,
  BatteryInfo = 0x01,
};

void packet_decoder(FILE* fp);

/**
 * @brief Outputs valid battery information.
 *
 * @param pStruct Pointer to the BatteryInfoStruct_t to parse.
 * @param stream Output filestream to use.
 */
void print_battery_packet(pBatteryInfoStruct_t pStruct, FILE* stream) {
  fprintf(stream, "B;" TIME_FMT ";%s\n", (double)pStruct->time_ms / 1e3,
          BatteryStateString[pStruct->battery_status]);
}

/**
 * @brief Outputs valid power sensor transitions.
 *
 * Keeps track of the time spent in a new state.
 * If the state transition is valid and the time spent meets the threshold,
 * the transition is output.
 *
 * @param prev_pStruct Pointer to the previous PowerLevelStruct packet.
 *                     The value of the pointer is updated to match
 *                     pStruct at the end of the function.
 * @param pStruct Pointer to the current PowerLevelStruct packet.
 * @param stream Output filestream to use.
 */
void print_power_packet(pPowerLevelStruct_t prev_pStruct,
                        pPowerLevelStruct_t pStruct, FILE* stream) {
  //   static pPowerLevelStruct_t previous_packet = PREVIOUS_PACKET_CONST;
  static uint8_t current_state = 0;
  static uint8_t new_state = 0;
  static uint32_t time_in_new_state_ms = 0;
  const uint8_t transition_threshold_ms = 10;

  // if the state is going to change
  if ((power_state(pStruct) != current_state)) {
    new_state = power_state(pStruct);
    // and the transition is allowed
    if (valid_power_state_transition(current_state, new_state)) {
      // increment the time spent in the new state
      time_in_new_state_ms += ((pStruct->time_ms) - (prev_pStruct->time_ms));
    } else {
      time_in_new_state_ms = 0;
    }
  }
  // if we have been in the state for at least the threshold time
  if (time_in_new_state_ms >= transition_threshold_ms) {
    fprintf(stream, "S;" TIME_FMT ";%i-%i\n",
            (double)(pStruct->time_ms - time_in_new_state_ms) / 1e3,
            current_state, new_state);
    // We are now in the new state, and we can reset the timer
    current_state = new_state;
    time_in_new_state_ms = 0;
  }
  *prev_pStruct = *pStruct;  // update the value of the previous packet
}

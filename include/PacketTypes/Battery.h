#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

// We do not want any alignment padding so we can typecast this
#pragma pack(push, 1)
/**
 * @brief The data from a battery information packet.
 *
 */
typedef struct {  // NOLINT(altera-struct-pack-align)
  uint32_t time_ms;
  uint8_t battery_status;
  uint8_t error_check;

} BatteryInfoStruct_t, *pBatteryInfoStruct_t;
#pragma pack(pop)  // resore previous pack value

/**
 * @brief The valid battery states.
 *
 */
enum BatteryState {
  VLOW,
  LOW,
  MED,
  HIGH,
};

/**
 * @brief A map from battery state enum to the appropriate output string.
 *
 */
static const char* BatteryStateString[] = {
    "VLOW",
    "LOW",
    "MED",
    "HIGH",
};

/**
 * @brief Converts Network Byte Order fields to Host Byte Order.
 *
 * @param pStruct Pointer to the BatteryInfoStruct.
 */
void convert_battery(pBatteryInfoStruct_t pStruct) {
  pStruct->time_ms = _byteswap_ulong(pStruct->time_ms);
}

/**
 * @brief True if packets computed error check matches the expected.
 *
 * @param packet_type The byte corresponding to the packet type.
 * @param pStruct The BatteryInfoStruct to compute the error check for.
 * @return true If the error check is correct.
 * @return false If the error check is incorrect.
 */
bool is_battery_packet_error_free(uint8_t packet_type,
                                  pBatteryInfoStruct_t pStruct) {
  uint32_t computed_error_check =
      (packet_type + pStruct->battery_status + pStruct->time_ms) % 256;
  return pStruct->error_check == computed_error_check;
}

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// We do not want any alignment padding so we can typecast this
#pragma pack(push, 1)
/**
 * @brief The data from a power level sensor packet.
 *
 */
typedef struct {  // NOLINT(altera-struct-pack-align)
  uint32_t time_ms;
  uint32_t voltage_V;
  uint64_t current_mA;
  uint8_t error_check;

} PowerLevelStruct_t, *pPowerLevelStruct_t;
#pragma pack(pop)  // resore previous pack value

/**
 * @brief Converts Network Byte Order fields to Host Byte Order.
 *
 * @param pStruct Pointer to the PowerLevelStruct.
 */
void convert_power(pPowerLevelStruct_t pStruct) {
  pStruct->time_ms = _byteswap_ulong(pStruct->time_ms);
  pStruct->voltage_V = _byteswap_ulong(pStruct->voltage_V);
  pStruct->current_mA = _byteswap_uint64(pStruct->current_mA);
}
/**
 * @brief Return the power level state for the PowerLevelStruct packet.
 *
 * @param pStruct The PowerLevelStruct packet to compute the power level state.
 * @return uint8_t The power level state.
 */
uint8_t power_state(pPowerLevelStruct_t pStruct) {
  uint64_t power = pStruct->voltage_V * pStruct->current_mA;
  uint8_t state = 0;

  if (power < 300) {
    state = 0;
  } else if (power < 550) {
    state = 1;
  } else if (power < 800) {
    state = 2;
  } else if (power < 1200) {
    state = 3;
  }
  return state;
}

/**
 * @brief True if the state transition is valid.
 *
 * In place of a full state machine, we keep a tab on the valid
 * state transitions.
 *
 * @param current Current power state.
 * @param next Power state to transition to.
 * @return true If the state transition is allowed.
 * @return false If the state transition is not allowed.
 */
bool valid_power_state_transition(uint8_t current, uint8_t next) {
  return ((current == 0 && next == 1)     // Starting
          || (current == 1 && next == 2)  // Warm Up
          || (current == 2 && next == 3)  // Main Session
          || (current == 3 && next == 2)  // Cool Down
          || (current == 2 && next == 0)  // Complete
  );
}

/**
 * @brief True if packets computed error check matches the expected.
 *
 * @param packet_type The byte corresponding to the packet type.
 * @param pStruct The PowerLevelStruct to compute the error check for.
 * @return true If the error check is correct.
 * @return false If the error check is incorrect.
 */
bool is_power_packet_error_free(uint8_t packet_type,
                                pPowerLevelStruct_t pStruct) {
  uint32_t computed_error_check = (packet_type + pStruct->time_ms +
                                   pStruct->current_mA + pStruct->voltage_V) %
                                  256;
  return pStruct->error_check == computed_error_check;
}
